<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConsultorioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consultorio', function (Blueprint $table) {
            $table->id();
            $table->integer('numero_consultorio');
            $table->string('medico_atiende');
            $table->string('clave_unica');
            $table->unsignedBigInteger('hospital_id');
            $table->timestamps();

            $table->foreign("hospital_id")->references("id")->on("hospital");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consultorio');
    }
}
