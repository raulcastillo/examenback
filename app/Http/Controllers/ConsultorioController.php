<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ConsultorioModel;

class ConsultorioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ConsultorioModel::with("hospital")->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    
        try {

            $consultorio = new ConsultorioModel();

            $consultorio->numero_consultorio = $request->numero_consultorio;
            $consultorio->medico_atiende = $request->medico_atiende;
            $consultorio->clave_unica = $request->clave_unica;
            $consultorio->hospital_id = $request->hospital_id;
            $consultorio->save();

            $result["status"] = true;
        } catch (Exception $e) {
            $result["status"] = false;
            $result["msg"] = "SE HA GENERADO UN ERROR INTERNO";
            $result["error"] = $e->getMessage();
        }

        return $result;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            $consultorio = ConsultorioModel::where('id',$request->id)->first();

            $consultorio->numero_consultorio = $request->numero_consultorio;
            $consultorio->medico_atiende = $request->medico_atiende;
            $consultorio->clave_unica = $request->clave_unica;
            $consultorio->hospital_id = $request->hospital_id;
            $consultorio->save();

            $result["status"] = true;
        } catch (Exception $e) {
            $result["status"] = false;
            $result["msg"] = "SE HA GENERADO UN ERROR INTERNO";
            $result["error"] = $e->getMessage();
        }

        return $result;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $consultorio = ConsultorioModel::where('id',$id)->delete();
            $result['status']=true;
           // $result['lineas']= $linea;
        }catch(Exception $e){
            $result['status'] = false;
            $result['msg'] = "hubo un error inminente";
            $result['error']= $e->getMessage();
        }
        return $result;
    }
}
