<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\HospitalModel;
use App\Models\DireccionModel;
use Illuminate\Support\Facades\DB;

class HospitalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return HospitalModel::with("direccion")->with("consultorios")->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $hospital = new HospitalModel();
            $direccion = new DireccionModel();
            
            $direccion->calle = $request->calle;
            $direccion->cruzamiento = $request->cruzamiento1;
            $direccion->cruzamiento2 = $request->cruzamiento2;
            $direccion->codigo_postal = $request->codigo_postal;
            $direccion->save();

            $hospital->unidad_medica = $request->unidad_medica;
            $hospital->nombre_encargado = $request->nombre_encargado;
            $hospital->clave_unica = $request->clave_unica;
            $hospital->direccion_id = $direccion->id;
            $hospital->save();

            $result["status"] = true;
        } catch (Exception $e) {
            $result["status"] = false;
            $result["msg"] = "SE HA GENERADO UN ERROR INTERNO";
            $result["error"] = $e->getMessage();
        }

        return $result;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $hospital = HospitalModel::where('id',$request->id)->first();
            $direccion = DireccionModel::where('id',$request->direccion_id)->first();
            $direccion->calle = $request->calle;
            $direccion->cruzamiento = $request->cruzamiento1;
            $direccion->cruzamiento2 = $request->cruzamiento2;
            $direccion->codigo_postal = $request->codigo_postal;
            $direccion->save();

            $hospital->unidad_medica = $request->unidad_medica;
            $hospital->nombre_encargado = $request->nombre_encargado;
            $hospital->clave_unica = $request->clave_unica;
            $hospital->save();

            $result['status']=true;
           // $result['lineas']= $linea;
        }catch(Exception $e){
            $result['status'] = false;
            $result['msg'] = "hubo un error inminente";
            $result['error']= $e->getMessage();  
        }
        return $result;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $hospital = HospitalModel::where('id',$id)->delete();
            $result['status']=true;
           // $result['lineas']= $linea;
        }catch(Exception $e){
            $result['status'] = false;
            $result['msg'] = "hubo un error inminente";
            $result['error']= $e->getMessage();
        }
        return $result;
    }
}
