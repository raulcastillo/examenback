<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\DireccionModel;
use App\Models\ConsultorioModel;



class HospitalModel extends Model
{
    protected $table = 'hospital';
    
    public function direccion(){
        
        return $this->belongsTo(DireccionModel::class,"direccion_id","id");
        
    }

    public function consultorios(){

        return $this->hasMany(ConsultorioModel::class,"hospital_id","id");
    }
}