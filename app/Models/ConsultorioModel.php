<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\DireccionModel;
use App\Models\HospitalModel;



class ConsultorioModel extends Model
{
    protected $table = 'consultorio';
    
    public function hospital(){
        
        return $this->belongsTo(HospitalModel::class,"hospital_id","id");
        
    }

    // public function consultorios(){

    //     return $this->hasMany(ConsultorioModel::class,"hospital_id","id");
    // }
}